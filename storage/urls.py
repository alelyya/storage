"""storage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from views import base, success, failture, upload, dirmaker, home, delete, download
from django.conf.urls import include
from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm


urlpatterns = [
    url(r'^$', home),
    url(r'^register/$', CreateView.as_view(template_name = 'registration/register.html',
                                           form_class = UserCreationForm,
                                           success_url = '/')),
    url(r'^admin/', admin.site.urls),
    url(r'^base/(\w*)$', base), url(r'^success/$', success),
    url(r'^failture/$', failture), url(r'^upload/(\w*)$', upload),
    url(r'^dirmaker/$', dirmaker), url(r'^delete/([\w/.]*)$', delete),
    url(r'^download/([\w/.]*)$', download),
    url('^', include('django.contrib.auth.urls'))
]
