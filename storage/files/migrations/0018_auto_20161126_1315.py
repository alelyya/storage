# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-26 13:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('files', '0017_auto_20161126_1314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='last_modified',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True),
        ),
    ]
