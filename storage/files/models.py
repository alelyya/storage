from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
import os


class Folder(models.Model):
    name = models.CharField(max_length = 50)
    parent = models.ForeignKey('self', null=True)
    owner = models.ForeignKey(User)

    def delete_full(self):
        subfolders = Folder.objects.filter(parent = self)
        for sub in subfolders:
            sub.delete_full()
        for file in File.objects.filter(parent = self):
            file.delete_full()
        self.delete()


class File(models.Model):
    name = models.CharField(max_length = 150)
    path = models.CharField(max_length = 150)
    extension = models.CharField(max_length = 50)
    hash_file = models.CharField(max_length = 32)
    size = models.BigIntegerField()
    parent = models.ForeignKey(Folder)
    owner = models.ForeignKey(User)
    description = models.TextField(default = '', blank = True, null=True)
    last_modified = models.DateTimeField(auto_now_add=True, blank = True, null=True)

    def delete_full(self):
        user_p = UserProfile.objects.get(user = self.owner)
        user_p.disk_usage -= self.size
        user_p.save()

        os.remove('/base/'+self.path)
        self.delete()


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='user')
    disk_usage = models.BigIntegerField(default = 0)
    max_disk_usage = models.BigIntegerField(default = 1073741824)


def create_base(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        user_profile = UserProfile(user=user)
        user_profile.save()
        user_base_folder = Folder(name = 'base', parent = None, owner = user)
        user_base_folder.save()
post_save.connect(create_base, sender=User)
