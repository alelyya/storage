import os
from storage.files.models import File, UserProfile


def disk_upload(files, current_user, current_folder):
    errors = {'already_uploaded':[]}
    for file in files:
        error = None
        file_info = {'name' : file[0], 'content-type' : file[1], 'size' : file[2], 'path' : file[3], 'md5' : file[4]}
    
        file_extension = file_info['name'].split('.')[-1]
        hashed_path = create_hashed_path(current_user.username,current_folder.name,file_info['name'])
        local_path = get_local_path(hashed_path, file_extension)
    
        db_content = File.objects.filter(parent = current_folder)
        for other_file in db_content:
            if other_file.path == local_path:
                del_from_disc(file_info['path'])
                error = file_info['name']
                break
        
        if error:
            errors['already_uploaded'].append(error)
            continue

        mov_and_rename(file_info['path'], local_path)

        user_p = UserProfile.objects.get(user = current_user)
        user_p.disk_usage += int(file_info['size'])
        user_p.save()
    
        upfile = File(name=file_info['name'], path=local_path, extension = file_extension,
                      hash_file = file_info['md5'], size = int(file_info['size']),
                      parent=current_folder, owner = current_user)
        upfile.save()
    return errors


###############  SHORTCUTS  ###############


def hashmd5(string):
    import hashlib
    h = hashlib.new('md5')
    h.update(bytes(string, encoding='UTF-8'))
    return h.hexdigest()


def create_hashed_path(user_name, folder_name, file_name):
    return hashmd5(user_name+folder_name+file_name)


def get_local_path(hashed_path, file_extension):
    return ('%s/%s/%s/' % (hashed_path[0], hashed_path[1], hashed_path[2])+hashed_path[3:])+'.'+file_extension


def del_from_disc(tmp_path):
    os.remove(tmp_path)


def mov_and_rename(tmp_path, new_path):
    os.rename(tmp_path, '/base/'+new_path)


def disk_makedirs():
    base = '/base/tmp'
    dirc_names = [hex(i)[-1] for i in range(16)]
    for x in dirc_names:
        for y in dirc_names:
            for z in dirc_names:
                os.makedirs(base+'%s/%s/%s/' % (x,y,z))
