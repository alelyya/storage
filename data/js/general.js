$(window).load(function() {
    $('#slider-nivo').nivoSlider({
        effect: 'fade',
        animSpeed: 500,
        pauseTime: 6000,
        directionNav: false,
        controlNav: false,
        keyboardNav: false,
        pauseOnHover: false
    });
});
