function OpenImageOverlay(imgpath, filepath, name, size, owner, last_modified) {
  if (imgpath == '/open/'+filepath) { 
    var imagePath = "<a href='/open/"+filepath+
                  "'title='Click to zoom in'><img src='"+imgpath+"'></a>";
        document.getElementById("fileImageSub").innerHTML = '';
        document.getElementById("fileImage").innerHTML = imagePath;}
  else {
    var imagePath = "<a href='/open/"+filepath+
                  "'title='Click to open'><img src='"+imgpath+"' width='256'></a>";
        document.getElementById("fileImage").innerHTML = '';
        document.getElementById("fileImageSub").innerHTML = imagePath;}

  document.getElementById("fileDelDown").innerHTML = "<a href='/download/"+filepath+"'>[Download]</a><a href='/delete/"+filepath+"'>[Delete]</a>";
  document.getElementById("fileName").innerHTML = name;
  document.getElementById("fileSize").innerHTML = size;
  document.getElementById("fileOwner").innerHTML = owner;
  document.getElementById("fileLastModified").innerHTML = last_modified;
  document.getElementById("file_overlay").style.visibility = "visible";
}