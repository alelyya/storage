from django.http import HttpResponse, HttpResponseRedirect
from storage.files.models import File, Folder, UserProfile
from django.shortcuts import render
from forms import UploadFileForm, CreateFolderForm
from django.contrib.auth.forms import AuthenticationForm
from django.views.decorators.csrf import csrf_exempt
from django.utils.http import urlquote
from storage.files.handle_local import disk_upload, disk_makedirs


def base(request, current_folder_id):
    current_user = request.user
    if not current_user.is_authenticated():
        return HttpResponseRedirect('/')
    
    current_user_p = UserProfile.objects.get(user = current_user)
    
    try:
        current_folder = Folder.objects.get(id=current_folder_id)
    except:
        current_folder = Folder.objects.get(parent = None, owner = current_user)
    
    if current_folder.owner != current_user:
        return HttpResponseRedirect('/')
    else:
        files = File.objects.filter(parent=current_folder, owner = current_user).order_by('name')
        folders = Folder.objects.filter(parent=current_folder, owner = current_user).order_by('name')
        
    upload_form = UploadFileForm()
    create_form = CreateFolderForm()

    if request.method == 'POST':
        if 'folder_create' in request.POST:
            create_form = CreateFolderForm(request.POST)
            if create_form.is_valid():
                return create_folder(request, current_folder)

    return render(request, 'index.html', {'upload_form':upload_form, 'create_form':create_form, 'files':files,
                                          'current_folder':current_folder, 'folders':folders,
                                          'user':current_user, 'user_p':current_user_p})


def home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/base/')
    loginform = AuthenticationForm()
    return render(request, 'home.html', {'form':loginform})


def download(request, filepath):
    file = File.objects.get(path = filepath)
    return HttpResponse(urlquote(file.name), content_type = 'application/octet-stream')


@csrf_exempt
def upload(request, current_folder_id):
    
    current_user = request.user
    current_folder = Folder.objects.get(id = current_folder_id)

    name_list = request.POST.getlist('file.name')
    content_type_list = request.POST.getlist('file.content_type')
    size_list = request.POST.getlist('file.size')
    path_list = request.POST.getlist('file.path')
    md5_list = request.POST.getlist('file.md5')
    
    files = list(zip(name_list, content_type_list, size_list, path_list, md5_list))
    
    errors = disk_upload(files, current_user, current_folder)

    return HttpResponseRedirect('/base/'+str(current_folder.id))


def delete(request, delete_id):
    try:
        obj = File.objects.get(path = delete_id)
    except:
        try:
            obj = Folder.objects.get(id = delete_id)
        except:
            return HttpResponseRedirect('/base/')
    current_folder = obj.parent
    if obj.owner == request.user and current_folder:
        obj.delete_full()
    else:
        return HttpResponseRedirect('/base/')
    return HttpResponseRedirect('/base/'+str(current_folder.id))


def create_folder(request, current_folder):
    new_folder_name = request.POST['name']
    current_user = request.user
    if Folder.objects.filter(name=new_folder_name, parent=current_folder, owner = current_user):
        return failture(request)
    new_folder = Folder(name=new_folder_name, parent=current_folder, owner = current_user)
    new_folder.save()
    return HttpResponseRedirect('/base/'+str(current_folder.id))

#######################################


def success(request):
    return HttpResponse("<b>SUCCESS</b>")


def failture(request):
    return HttpResponse("<b>FAILTURE</b>")

#####################################


def dirmaker(request):
    disk_makedirs()
    return HttpResponse("<b>DONE</b>")
