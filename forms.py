from django import forms


class UploadFileForm(forms.Form):
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))


class CreateFolderForm(forms.Form):
    name = forms.CharField(max_length = 50)
